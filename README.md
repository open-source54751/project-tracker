# Project Tracker

Simple project to keep track of the projects I have running on my home server, and learn a bit about AspNet Razor projects

## Getting Started

This project is a Dotnet 8.0 project

![Home](./assets/screenshots/ProjectTracker.png)

### Prerequisites

- [Dotnet 8.0](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)

## Running Project

- In project root run: `dotnet restore`
- To run this project simply in project root execute: `dotnet run`

## License

This project is licensed under the Apache License 2.0
