using ProjectTracker;
using ProjectTracker.Components;
using Blazored.Modal;
using Microsoft.EntityFrameworkCore;
using ProjectTracker.Data.Database;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
DIHelpers.PrepareMySQL(builder);
builder.Services.AddBlazoredModal();
builder.Services
.AddRazorComponents()
.AddInteractiveServerComponents();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error", createScopeForErrors: true);
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseAntiforgery();

app.MapRazorComponents<App>()
    .AddInteractiveServerRenderMode();

var dbContext = app.Services.CreateScope().ServiceProvider.GetService<IDbContextFactory<ProjectsDbContext>>();

dbContext!.CreateDbContext().MigrateDb();

app.Run();

