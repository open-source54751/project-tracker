using Microsoft.EntityFrameworkCore;
using ProjectTracker.Data.Contracts;
using ProjectTracker.Data.Database;
using ProjectTracker.Data.Repositories;

namespace ProjectTracker;

public static class DIHelpers
{
    public static void PrepareMySQL(WebApplicationBuilder? builder)
    {
        var CONNECTION_STRING = builder?.Configuration.GetSection("ConnectionStrings").GetValue<string>("mysql");
        var MYSQL_SERVER_VERSION = new MySqlServerVersion(new Version(8, 0, 29));
        builder?.Services
        .AddDbContextFactory<ProjectsDbContext>(x =>
        {
            x.UseMySql(CONNECTION_STRING, MYSQL_SERVER_VERSION);
            x.LogTo(Console.WriteLine, LogLevel.Information);
            x.EnableSensitiveDataLogging();
            x.EnableDetailedErrors();
        });

        builder?.Services.AddSingleton<IProjectRepository, ProjectRepository>();
    }
}