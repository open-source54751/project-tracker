using System.ComponentModel.DataAnnotations;

namespace ProjectTracker.Data.Models;

public class Project()
{
    public Project(Guid id, string title, string description, DateTime createdAt, string link) : this()
    {
        Id = id;
        Title = title;
        Description = description;
        CreatedAt = createdAt;
        Link = link;
    }
    public Guid Id { get; set; }
    [Required]
    [StringLength(20, ErrorMessage = "Title is too long")]
    public string Title { get; set; }
    [Required]
    [StringLength(100, ErrorMessage = "Description is too long")]
    public string Description { get; set; }

    [Required(AllowEmptyStrings = false, ErrorMessage = "Link is required")]
    public string Link { get; set; }
    public DateTime CreatedAt { get; set; }
}