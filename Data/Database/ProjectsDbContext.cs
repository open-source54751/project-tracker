using Microsoft.EntityFrameworkCore;
using ProjectTracker.Data.Models;

namespace ProjectTracker.Data.Database;

public class ProjectsDbContext : DbContext
{
    public DbSet<Project> Projects { get; set; }
    public ProjectsDbContext(DbContextOptions<ProjectsDbContext> options) : base(options)
    {
        MigrateDb();
    }

    public void MigrateDb()
    {
        Database.EnsureCreated();
        var hasPendingMigrations = Database.GetPendingMigrations();
        if (Database.HasPendingModelChanges() || hasPendingMigrations.Any())
        {
            Database.Migrate();
        }
    }
}