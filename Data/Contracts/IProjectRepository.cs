using ProjectTracker.Data.Models;

namespace ProjectTracker.Data.Contracts;

public interface IProjectRepository : IDisposable
{
    Task<Project> GetByIdAsync(Guid projectId);
    Task<IList<Project>> GetProjectsAsync(int skip, int pageSize);
    Task<Project> AddAsync(Project project);
    Task<Project> DeleteAsync(Project project);
    Task<Project> UpdateAsync(Project project);
}