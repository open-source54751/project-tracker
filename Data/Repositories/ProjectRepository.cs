using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using ProjectTracker.Data.Contracts;
using ProjectTracker.Data.Database;
using ProjectTracker.Data.Models;

namespace ProjectTracker.Data.Repositories;

public class ProjectRepository : IProjectRepository
{
    private readonly ProjectsDbContext ProjectsContext;
    public ProjectRepository(IDbContextFactory<ProjectsDbContext> contextFactory)
    {
        ProjectsContext = contextFactory.CreateDbContext();
    }

    public async Task<Project> AddAsync(Project project)
    {
        var newProject = ProjectsContext.Add(project);
        await ProjectsContext.SaveChangesAsync();
        return newProject.Entity;
    }

    public async Task<Project> DeleteAsync(Project project)
    {
        ProjectsContext.Projects.Remove(project);
        await ProjectsContext.SaveChangesAsync();
        return project;
    }

    public void Dispose()
    {
        ProjectsContext.Dispose();
        GC.SuppressFinalize(this);
    }

    public Task<Project> GetByIdAsync(Guid projectId)
    {
        throw new NotImplementedException();
    }

    public async Task<IList<Project>> GetProjectsAsync(int skip = 0, int pageSize = 10)
    {
        return await ProjectsContext.Projects.Skip(skip).Take(pageSize).ToListAsync();
    }

    public Task<Project> UpdateAsync(Project project)
    {
        throw new NotImplementedException();
    }
}