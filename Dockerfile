FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /app
COPY . .
EXPOSE 5026
ENTRYPOINT ["dotnet", "ProjectTracker.dll"]